﻿namespace ServerCommon.CacheRepository;

public enum CacheReturnCode : int
{
    UnknownError = -1,
    None = 0,
    Success = 1,
    UpdateFail,
    ReadFail,
    CastFail,
    NoData,
    ExpiredSessionId,
    DuplicationLogin,
}
