﻿namespace ServerCommon.CacheRepository;

public class CacheConnectionOption
{
    public const string CacheConnection = "CacheConnection";
    public string Host { get; set; }
    public int Port { get; set; }
    public string Password { get; set; }
    public int SessionCache { get; set; }
    public int GameCache { get; set; }
}
