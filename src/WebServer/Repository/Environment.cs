﻿namespace WebServer.Model.Option;

public class EnvironmentOption
{
    public const string Environment = "Environment";

    public string EnvironmentTypeString { get; set; }
}
