﻿namespace ServerCommon.CacheRepository.Interface;

public abstract class BaseCacheRepository
{
    public static int concurrentRequestCount = 0;

    protected int _maxRetries = 3;
    protected int _retryDelay = 500;

    protected abstract bool Connect();
    protected abstract bool IsConnected();


    protected virtual bool RetryCacheConnect()
    {
        try
        {
            if (IsConnected())
            {
                return true;
            }

            if (!Connect())
            {
                Console.WriteLine($"Init Cache Connect : Connect By Redis. Error");
                return false;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine($"Retry Cache Connect Exception : {e.ToString()}");
            return false;
        }

        return true;
    }

    protected void RunCommand(Action function)
    {
        int retries = 0;
        while (true)
        {
            try
            {
                function();
                return;
            }
            catch (Exception e)
            {
                Task.Delay(_retryDelay).Wait();
                RetryCacheConnect();
                if (++retries == _maxRetries)
                {
                    Console.WriteLine($"RunCommand Exception : {e.ToString()}");
                    return;
                }
            }
        }
    }

    protected async Task RunCommandAsync(Action function)
    {
        await Task.Run(async () =>
        {
            int retries = 0;

            while (true)
            {
                try
                {
                    function();
                    return;
                }
                catch (Exception e)
                {
                    if (++retries > _maxRetries)
                    {
                        Console.WriteLine($"RunCommandAsync Exception : {e.ToString()}");
                        return;
                    }

                    await Task.Delay(_retryDelay);
                    RetryCacheConnect();
                }
            }
        }).ConfigureAwait(false);
    }

    protected async Task<TResult> RunCommandAsync<TResult>(Func<Task<TResult>> function, TResult defaultResult)
    {
        return await Task.Run(async () =>
        {
            int retries = 0;

            while (true)
            {
                try
                {
                    return await function();
                }
                catch (Exception e)
                {
                    if (++retries > _maxRetries)
                    {
                        Console.WriteLine($"RunCommandAsync Exception: {e.ToString()}");
                        return defaultResult;
                    }

                    await Task.Delay(_retryDelay);
                    RetryCacheConnect();
                }
            }
        }).ConfigureAwait(false);
    }

    protected TResult RunCommand<TResult>(Func<TResult> function, TResult defaultResult)
    {
        int retries = 0;
        while (true)
        {
            try
            {
                return function();
            }
            catch (Exception e)
            {
                Task.Delay(_retryDelay).Wait();
                RetryCacheConnect();
                if (++retries != _maxRetries)
                {
                    continue;
                }

                Console.WriteLine($"RunCommand Exception: {e.ToString()}");
                return defaultResult;
            }
        }
    }
}
