﻿using System.Collections.Concurrent;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using ServerCommon.CacheRepository;
using ServerCommon.CacheRepository.Interface;
using StackExchange.Redis;
using WebServer.Model.Option;

namespace WebServer.Repository;

public class RedisCacheRepository : BaseCacheRepository
{
    private readonly int _sessionExpireSec = 60 * 3;            // 3 min
    private readonly int _tempUserSessionExpireSec = 60 * 3;    // 3 min
    private readonly int _userAggregateExpireSec = 60 * 3;      // 3 min
    private readonly int _reserveExpireSec = 60 * 3;            // 3 min
    private readonly int _lastUpdatedAtUpdateCount = 5;

    private readonly string _host;
    private readonly int _port;
    private readonly string _password;
    private readonly int _sessionCacheIndex;
    private readonly int _gameCacheIndex;
    private readonly ConfigurationOptions _option;
    private ConnectionMultiplexer _connection;

    private IDatabase _sessionCache;
    private IDatabase _gameCache;

    /// <summary>
    /// 유저별 UpdatedAt 캐시를 러프하게 갱신하기 위한 컨테이너
    /// </summary>
    private ConcurrentDictionary<string, int> _lastUpdatedAtUpdateCountMap = new();

    /// <summary> For Pub/Sub </summary>
    public ISubscriber Subscriber { get; }

    public RedisCacheRepository(IOptions<CacheConnectionOption> options, IOptions<EnvironmentOption> environmentOptions)
    {
        _host = options.Value.Host;
        _port = options.Value.Port;
        _password = options.Value.Password;
        _sessionCacheIndex = options.Value.SessionCache;
        _gameCacheIndex = options.Value.GameCache;

        _option = BuildRedisConfiguration();
        Connect();

        _sessionCache = _connection.GetDatabase(_sessionCacheIndex);
        _gameCache = _connection.GetDatabase(_gameCacheIndex);

        Subscriber = _connection.GetSubscriber();

        // 개발환경일 경우 만료시간 연장
        if (environmentOptions.Value.EnvironmentTypeString.Equals("Development"))
        {
            _sessionExpireSec *= 1000;
            _tempUserSessionExpireSec *= 1000;
            _userAggregateExpireSec *= 1000;
            _reserveExpireSec *= 1000;
        }
    }

    private ConfigurationOptions BuildRedisConfiguration()
    {
        return new()
        {
            AbortOnConnectFail = false,
            ConnectRetry = 5,
            ConnectTimeout = 3000,
            SyncTimeout = 5000,
            DefaultDatabase = 0,
            EndPoints = { { _host, _port } },
            Password = _password
        };
    }

    protected override bool Connect()
    {
        _connection = ConnectionMultiplexer.Connect(_option);
        return _connection.IsConnected;
    }

    protected override bool IsConnected()
    {
        return _connection.IsConnected;
    }

    public async Task<bool> CheckHealthState()
    {
        try
        {
            TimeSpan timeSpan = await _gameCache.PingAsync();
            if (timeSpan == TimeSpan.Zero)
            {
                return false;
            }
        }
        catch (Exception exception)
        {
            Console.WriteLine("World Redis CheckHealthCheck Fail");
            return false;
        }

        return true;
    }

    public async Task<CacheReturnCode> SetStringAsync(string key, string value, CommandFlags flags)
    {
        return await RunCommandAsync(async () =>
        {
            RedisValue redisValue = new RedisValue(value);
            bool isWrite = await _gameCache.StringSetAsync(key, redisValue, new TimeSpan(0, 0, _userAggregateExpireSec), When.NotExists, flags);
            if (!isWrite)
            {
                return CacheReturnCode.UpdateFail;
            }

            return CacheReturnCode.Success;
        }, CacheReturnCode.UpdateFail);
    }

    public async Task<string> GetStringAsync(string key, CommandFlags flags)
    {
        return await RunCommandAsync(async () =>
        {
            RedisValue value = await _gameCache.StringGetAsync(key, flags);
            return value == RedisValue.Null ? string.Empty : value.ToString();
        }, string.Empty);
    }

    #region ETC
    public async Task<CacheReturnCode> ResetAsync()
    {
        await _gameCache.ExecuteAsync("FLUSHDB");
        await _sessionCache.ExecuteAsync("FLUSHDB");
        return CacheReturnCode.Success;
    }

    #endregion
}
