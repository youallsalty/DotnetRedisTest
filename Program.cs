using dotnet_redis_test.Controllers;
using ServerCommon.CacheRepository;
using WebServer.Model.Option;
using WebServer.Repository;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

builder.Services.Configure<EnvironmentOption>(settings =>
{
    builder.Configuration.GetSection(EnvironmentOption.Environment).Bind(settings);
});

builder.Services.Configure<CacheConnectionOption>(settings =>
{
    builder.Configuration.GetSection(CacheConnectionOption.CacheConnection).Bind(settings);
});

builder.Services.AddSingleton<RedisCacheRepository, RedisCacheRepository>();
builder.Services.AddSingleton<LargeStringData, LargeStringData>();

builder.Services.AddSession();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseSession();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

Console.WriteLine("hey");

app.Run();
