﻿using System.Net.Mime;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using dotnet_redis_test.Models;
using WebServer.Repository;
using StackExchange.Redis;

namespace dotnet_redis_test.Controllers;

public class LargeStringData
{
    // public string PostHistory { get; set; } = System.IO.File.ReadAllText(Path.Combine("Data", "PostHistory.xml"));
    public string PostHistory { get; set; } = System.IO.File.ReadAllText(Path.Combine("Data", "MB_10.txt"));
    public string Data400Kb { get; set; } = System.IO.File.ReadAllText(Path.Combine("Data", "KB_400.txt"));
    public string Data250Kb { get; set; } = System.IO.File.ReadAllText(Path.Combine("Data", "KB_250.txt"));
    public string Data100Kb { get; set; } = System.IO.File.ReadAllText(Path.Combine("Data", "KB_100.txt"));

    public Dictionary<string, string> DataBySize { private set; get;}

    public LargeStringData()
    {
        Console.WriteLine($"LargeStringData() constructor!");

        this.DataBySize = new Dictionary<string, string>() {
            {"0" , string.Empty},
            {"10MB" , this.PostHistory},
            {"100KB", this.Data100Kb},
            {"250KB", this.Data250Kb},
            {"400KB", this.Data400Kb},
        };
    }
}

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly RedisCacheRepository _redisCacheRepository;
    private readonly LargeStringData _largeStringData;

    public HomeController(ILogger<HomeController> logger, RedisCacheRepository redisCacheRepository, LargeStringData largeStringData)
    {
        _logger = logger;
        _redisCacheRepository = redisCacheRepository;
        _largeStringData = largeStringData;
    }

    /// <summary>
    /// Index: 50ms : 25ms + 25ms
    /// WhenAll: 50ms
    /// Fire: 0ms
    /// Example: http://localhost:5050/home/index?fileSize=10MB
    /// </summary>
    /// <returns></returns>
    public async Task<IActionResult> Index(string filesize = "10MB")
    {
        if (_largeStringData.DataBySize.ContainsKey(filesize.ToUpper()) == false)
        {
            return NotFound();
        }

        bool state = await _redisCacheRepository.CheckHealthState();

        Stopwatch sw = new Stopwatch();

        sw.Start();

        string keyName = $"PostHistory.xml_{Guid.NewGuid().ToString("N")}";

        if (filesize != "0")
        {
            await _redisCacheRepository.SetStringAsync(keyName, _largeStringData.DataBySize[filesize.ToUpper()], CommandFlags.None); // adds 25ms
            string get = await _redisCacheRepository.GetStringAsync(keyName, CommandFlags.None); // adds 25ms
        }

        // ...

        sw.Stop();

        string result = $"Elapsed={sw.Elapsed.Milliseconds}ms for ${keyName}";

        Console.WriteLine(result);

        // return Ok(content);
        return Ok(result);
    }

    /// <summary>
    /// Note: Even if Set is None, still +25ms
    /// </summary>
    /// <returns></returns>
    public async Task<IActionResult> WhenAll()
    {
        bool state = await _redisCacheRepository.CheckHealthState();

        Stopwatch sw = new Stopwatch();

        sw.Start();

        // System.IO.File.WriteAllText(Path.Combine("Data", "PostHistory.xml"), "contents");

        // HttpContext.Session.Set("Foo", new byte[] { 1, 2, 3, 4, 5 });

        // string keyName = $"PostHistory.xml_{HttpContext.Session.Id}";
        string keyName = $"PostHistory.xml_{Guid.NewGuid().ToString("N")}";

        // Task task1 = _redisCacheRepository.SetStringAsync(keyName, _largeStringData.PostHistory, CommandFlags.None); // adds 25ms even if None
        Task task1 = _redisCacheRepository.SetStringAsync(keyName, _largeStringData.PostHistory, CommandFlags.FireAndForget); // adds 25ms
        Task task2 = _redisCacheRepository.GetStringAsync(keyName, CommandFlags.None); // adds 25ms

        // case 1: commented out
        // await Task.WhenAll(task1, task2); // 0ms if not awaited

        // case 2: await two times
        // await Task.WhenAll(task1, task2); // 50ms if awaited

        // case 3: only this
        await task2; // 50ms

        // ...

        sw.Stop();

        string result = $"Elapsed={sw.Elapsed.Milliseconds}ms for ${keyName}";

        Console.WriteLine(result);

        // return Ok(content);
        return Ok(result);
    }

    public async Task<IActionResult> Fire()
    {
        bool state = await _redisCacheRepository.CheckHealthState();

        Stopwatch sw = new Stopwatch();

        sw.Start();

        // System.IO.File.WriteAllText(Path.Combine("Data", "PostHistory.xml"), "contents");

        // HttpContext.Session.Set("Foo", new byte[] { 1, 2, 3, 4, 5 });

        // string keyName = $"PostHistory.xml_{HttpContext.Session.Id}";
        string keyName = $"PostHistory.xml_{Guid.NewGuid().ToString("N")}";

        await _redisCacheRepository.SetStringAsync(keyName, _largeStringData.PostHistory, CommandFlags.FireAndForget); // 0 ms
        string get = await _redisCacheRepository.GetStringAsync(keyName, CommandFlags.FireAndForget); // 0 ms

        // ...

        sw.Stop();

        string result = $"Elapsed={sw.Elapsed.Milliseconds}ms for ${keyName}";

        Console.WriteLine(result);

        // return Ok(content);
        return Ok(result);
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
