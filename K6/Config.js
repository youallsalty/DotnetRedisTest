// LOCAL - CH
// export const TARGET_IP = "10.28.206.189";
// export const TARGET_PORT = "5000";

// LOCAL - YS
// export const TARGET_IP = "10.28.206.174";
// export const TARGET_PORT = "5000";
// export const TARGET_PORT = "5050";

// QA2
export const TARGET_IP = "3.36.128.250";
export const TARGET_PORT = "5050";
// export const TARGET_IP = "13.125.174.95";
// export const TARGET_PORT = "12011";

export const TARGET_BASE_URL = `http://${TARGET_IP}:${TARGET_PORT}`;