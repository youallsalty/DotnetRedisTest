export const ClientInfo = {
    SessionId: (String = ""),
    EventSourcingVersion: (Number = 0),

    AccountId: (Number = 0),
    Nickname: (String = ""),

    IsPvpGroupJoined: false,
};
