import { check, group, sleep } from "k6";
import http from "k6/http";

import * as Config from "./Config.js";
import * as Models from "./Models.js";

let _clientInfo = Models.ClientInfo;

export const options = {
    vus: 50, // virtual user count
    // iterations: 300, // 200 * 10MB = 2GB
    duration: "s",
    summaryTrendStats: [
        "avg",
        "min",
        "med",
        "max",
        "p(95)",
        "p(99)",
        "p(99.99)",
        "count",
    ], // output options
};

export function setup() {
    Log("OnSetup");
}

export default function () {
    // Error(__VU);

    group("LargeText", function () {
        // Scenario_LargeText("400KB");
        // Scenario_LargeText("100KB");
        // Scenario_LargeText("250KB");
        Scenario_LargeText("10MB");
    });

    sleep(1);
}

//#region Functions
function Log(msg) {
    let prefix = `${_clientInfo.Nickname}`;
    console.log(`[${prefix}] ${msg}`);
}

function Error(msg) {
    let prefix = `${_clientInfo.Nickname}`;
    console.error(`[${prefix}] ${msg}`);
}

function API_LargetText(filesize) {
    const url = `${Config.TARGET_BASE_URL}/home/index?fileSize=${filesize}`;
    const res = http.get(url);

    return res;
}

function Scenario_LargeText(filesize) {
    Log("START Scenario_LargeText()");

    let res = API_LargetText(filesize);

    return res;
}
