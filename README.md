### To Test:

net7.0

```
dotnet run
```

```
cd K6
K6 run Main.js
```

### Configs

- .Net Port : `appsettings.json -> urls`
- K6 Target : `K6/Config.js`
- K6 VUsers, Iterations : `K6/Main.js -> const options`

### Test URLs

- http://localhost:5050/home/index?fileSize=100KB
- http://localhost:5050/home/index?fileSize=250KB
- http://localhost:5050/home/index?fileSize=10MB
- http://localhost:5050/home/index?fileSize=10MB

- http://13.125.174.95:12011/home/index?fileSize=100KB

See `HomeController.Index()`